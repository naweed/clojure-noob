(ns clojure-noob.core
  (:gen-class)
  (:require [clojure-noob.type :as te]
  			[clojure-noob.direct :as de])
)


(defn -main
  [& args]

  	(if (= (first args) "direct")
  		(de/-main)
  		(te/-main)
  	)
  )